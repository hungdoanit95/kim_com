<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_header')){
class TNCP_nd_kimcom_header extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_header" class=" wow fadeInUp" data-wow-duration="1.8s">
        <div class="navbar nd_kimcom_header_fixed">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-sm-4 col-xs-12 logo">
                        <a href="#"><img src="<?= $this->getPath() ?>images/logo.png" alt="logo" title="Kimcom"></a>
                    </div>
                    <div class="col-sm-8 col-xs-12 info f-wrap j-around">
                <span class="hotline">
                    <a href="tel: 0965.828.220">Hotline: 0965.828.220</a>
                </span>
                        <span class="email">
                    <a href="mailto: kimcomhcm@gmail.com" title="EMail">kimcomhcm@gmail.com</a>
                </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="menu-mb">
                <a href="#menu"><i class="fa fa-list"></i></a>
            </div>
            <div class="hidden-xs">
                <div id="menu">
                    <ul>
                        <li><a href="#">Trang chủ</a></li>
                        <li><a href="#">Giới thiệu</a></li>
                        <li><a href="#">Dịch vụ</a>
                            <ul>
                                <li><a href="#">Dịch vụ hosting</a></li>
                                <li><a href="#">Thiết kế web</a></li>
                                <li><a href="#">SEO</a></li>

                            </ul>
                        </li>
                        <li><a href="#">Báo giá</a></li>
                        <li><a href="#">Dự án</a></li>
                        <li><a href="#">Khách hàng</a></li>
                        <li><a href="#">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
            <div class="slider">
                <div class="content">
                    <h5>Chào mừng quý khách đến với</h5>
                    <h2>CÔNG TY TƯ VẤN & THIẾT KẾ THƯƠNG HIỆU KIMCOM</h2>
                    <div class="submit">
                        <a href="#" class="tuvan">Đặt lịch tư vấn</a>
                    </div>
                    <div class="submit">
                        <a href="#" class="dichvu">Xem tất cả dịch vụ</a>
                    </div>
                </div>

                <div class="section">
            <span class="indie ">
                <a href="#" class="align-items-center">
                        <img src="<?= $this->getPath() ?>images/icon-indie.png" alt="Ý tưởng" title="Ý tưởng">
                        <i class="fa fa-arrow-left"></i>
                        <span>
                            Tôi có ý tưởng!
                        </span>
                </a>
            </span>

                    <div class="mxh">
                        <a href="#">
                            <img src="<?= $this->getPath() ?>images/love.png" alt="">

                        </a>
                        <a href="#">
                            <img src="<?= $this->getPath() ?>images/user.png" alt="">
                        </a>
                        <a href="#">
                            <img src="<?= $this->getPath() ?>images/phone.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="click-slide">
                <span id="bottom">
                <img src="<?= $this->getPath() ?>images/icon-1.png" alt="cuộn xuống" title="cuộn xuống">
            </span>
            </div>
        </div>
    </section>
<?php }
}
}
