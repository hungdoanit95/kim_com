<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_footer-css' type="text/css" href="nd_kimcom_footer/assets/css.css"/>

    <section id="nd_kimcom_footer" class=" wow fadeInLeft" data-wow-duration="1.8s">
        <div class="container">
            <div class="col-sm-4 col-xs-12 ft-1">
                <h2>Dịch Vụ Tiêu Biểu</h2>
                <ul>
                    <li><a href="#">Thiết kế logo</a></li>
                    <li><a href="#">In ấn</a></li>
                    <li><a href="#">Thiết kế web</a></li>
                    <li><a href="#">Thiết kế thương hiệu</a></li>
                    <li><a href="#">Quay video</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12 ft-2">
                <h2>Về chúng tôi</h2>
                <ul>
                    <li><a href="#">Về KINGCOM</a></li>
                    <li><a href="#">Đội ngũ nhân sự</a></li>
                    <li><a href="#">Hình ảnh hoạt động</a></li>
                    <li><a href="#">Bản tin KIMCOM</a></li>
                    <li><a href="#">Cơ hội việc làm</a></li>
                </ul>
            </div>
            <div class="col-sm-5 col-xs-12 ft-3">
                <h2>
                    CTY TƯ VẤN & THƯƠNG HIỆU KIMCOM
                </h2>
                <ul>
                    <li>Trụ sở 1: 68 Nguyễn Huệ, P.Bến Nghé, Q1, TP.HCM  </li>
                    <li>Trụ sở 2: 125 Đồng Văn Cống, P. Đặng Mỹ Lợi, Q2, TP.HCM  </li>
                    <li>Email: <a href="mailto:kimcomhcm@gmail.com">kimcomhcm@gmail.com</a> </li>
                    <li>Hotline : <a href="tel:+0965 82 82 20">0965 82 82 20</a></li>
                    <li>Hotline 2 : <a href="tel:+ 0000000000">0000000000</a></li>
                    <li>Giờ làm việc: Thứ 2 - Thứ 6</li>
                </ul>
            </div>
        </div>
    </section>
    </div>
<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_footer/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';