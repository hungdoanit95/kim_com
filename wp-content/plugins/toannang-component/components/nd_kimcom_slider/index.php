<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_slider-css' type="text/css" href="nd_kimcom_slider/assets/css.css"/>

    <section id="nd_kimcom_slider">
        <div class="container">
            <h2 class="title wow fadeInUp" data-wow-duration="1s">
                các dự án lớn của kimcom
            </h2>
            <div class="slider wow fadeInUp" data-wow-duration="3s">
                <div class="item">
                    <a href="#">
                        <img src="nd_kimcom_slider/images/1.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="nd_kimcom_slider/images/3.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="nd_kimcom_slider/images/3.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="nd_kimcom_slider/images/1.png" alt="a" title="a">
                    </a>
                </div>
            </div>
            <div class="info wow fadeInUp" data-wow-duration="4s">
                <h2>Lý do mà các khách hàng lớn chọn KIMCOM</h2>
                <p>Kimcom đã xây dựng thương hiệu cho hơn 1000 doanh nghiệp
                    tại Việt Nam. Mục tiêu của KIMCOM là đưa thương hiệu việt nam lên một
                    tầm cao mới! Hãy Cùng KIMCOM mang đến những giá trị cho thương hiệu
                    ViệT Nam trong thời đại mới.</p>
            </div>

        </div>
    </section>

<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_slider/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';