wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: true, // default
    live: true // default
});
wow.init();

$(document).ready(function() {
    $('#nd_kimcom_slider .slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        speed: 400,
        autoplay: true,
        autoplaySpeed: 6000,
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        dots: false,
        responsive: [{
            breakpoint: 800,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                centerMode: false,
            }
        },
        
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false
            }
        }
    ]
    })
})