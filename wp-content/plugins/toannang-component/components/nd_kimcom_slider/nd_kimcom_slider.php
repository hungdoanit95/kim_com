<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_slider')){
class TNCP_nd_kimcom_slider extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_slider">
        <div class="container">
            <h2 class="title wow fadeInUp" data-wow-duration="1s">
                các dự án lớn của kimcom
            </h2>
            <div class="slider wow fadeInUp" data-wow-duration="3s">
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/1.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/3.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/3.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/1.png" alt="a" title="a">
                    </a>
                </div>
            </div>
            <div class="info wow fadeInUp" data-wow-duration="4s">
                <h2>Lý do mà các khách hàng lớn chọn KIMCOM</h2>
                <p>Kimcom đã xây dựng thương hiệu cho hơn 1000 doanh nghiệp
                    tại Việt Nam. Mục tiêu của KIMCOM là đưa thương hiệu việt nam lên một
                    tầm cao mới! Hãy Cùng KIMCOM mang đến những giá trị cho thương hiệu
                    ViệT Nam trong thời đại mới.</p>
            </div>

        </div>
    </section>
<?php }
}
}
