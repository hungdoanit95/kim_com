<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_customer-css' type="text/css" href="nd_kimcom_customer/assets/css.css"/>

    <section id="nd_kimcom_customer">
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="1s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="nd_kimcom_customer/images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="3s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="nd_kimcom_customer/images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="5s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="nd_kimcom_customer/images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="7s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="nd_kimcom_customer/images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_customer/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';