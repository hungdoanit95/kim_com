<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_customer')){
class TNCP_nd_kimcom_customer extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

 <section id="nd_kimcom_customer">
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="1s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="<?= $this->getPath() ?>images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="3s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="<?= $this->getPath() ?>images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="5s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="<?= $this->getPath() ?>images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 wow fadeInUp" data-wow-duration="7s">
                        <div class="card">
                            <div class="avatarr">
                                <img src="<?= $this->getPath() ?>images/1.jpg" alt="Bà a" title="Bà a">
                            </div>
                            <div class="info">
                                <h2>
                                    Nguyễn Trường Sơn
                                </h2>
                                <p>Giám đốc</p>
                                <div class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <div class="content">
                                <p>Kimcom thiết kế rất đúng ý tôi, tôi rất hài lòng và hiện giờ tôi vừa là khách hàng vừa là đối tác với Kimcom.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
}
}
