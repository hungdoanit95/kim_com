<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_hc_khangphuc_single_product')){
    class TNCP_hc_khangphuc_single_product extends TNCP_ToanNang{

        protected $options = [

        ];
        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        /*Add html to Render*/
        public function render(){ ?>
            <div id="hc_khangphuc_single_product">
                <div class="container">
                    <?php wc_get_template_part( 'content', 'single-product' ); ?>
                </div>

            </div>
        <?php }
}
}



