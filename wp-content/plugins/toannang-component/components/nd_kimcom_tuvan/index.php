<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_tuvan-css' type="text/css" href="nd_kimcom_tuvan/assets/css.css"/>

    <section id="nd_kimcom_tuvan">
       <div class="container">
           <div class="logo">
               <img src="nd_kimcom_tuvan/images/logo.png" alt="">
           </div>
          <div class="title">
              <h2 class="title-1 wow fadeInLeft" data-wow-duration="1.8s">
                  nhận tư vấn
              </h2>
              <h2 class="title-2 wow fadeInLeft" data-wow-duration="3.8s">
                  Miễn Phí Từ Chúng Tôi
              </h2>
          </div>

       </div>
        <div class="lienhe">
            <div class="container">
                <a href="#" class="lh-1 wow fadeInLeft" data-wow-duration="4.8s">Liên hệ ngay</a>
                <a href="#" class="lh-2 wow fadeInLeft" data-wow-duration="5.8s">Đặt lịch tư vấn</a>
            </div>
        </div>
    </section>

<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_tuvan/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';