<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_tuvan')){
class TNCP_nd_kimcom_tuvan extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_tuvan">
       <div class="container">
           <div class="logo">
               <img src="nd_kimcom_tuvan/images/logo.png" alt="">
           </div>
          <div class="title">
              <h2 class="title-1 wow fadeInLeft" data-wow-duration="1.8s">
                  nhận tư vấn
              </h2>
              <h2 class="title-2 wow fadeInLeft" data-wow-duration="3.8s">
                  Miễn Phí Từ Chúng Tôi
              </h2>
          </div>

       </div>
        <div class="lienhe">
            <div class="container">
                <a href="#" class="lh-1 wow fadeInLeft" data-wow-duration="4.8s">Liên hệ ngay</a>
                <a href="#" class="lh-2 wow fadeInLeft" data-wow-duration="5.8s">Đặt lịch tư vấn</a>
            </div>
        </div>
    </section>
<?php }
}
}
