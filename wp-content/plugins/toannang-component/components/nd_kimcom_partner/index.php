<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_partner-css' type="text/css" href="nd_kimcom_partner/assets/css.css"/>

    <section id="nd_kimcom_partner">
        <div class="la">
            <img src="nd_kimcom_partner/images/la.png" alt="">
        </div>
        <div class="dia">
            <img src="nd_kimcom_partner/images/dia.png" alt="">
        </div>
       <div class="container">
            <div class="doitac wow fadeInUp" data-wow-duration="1s">
                <a href="#">Đối Tác</a>
            </div>
           <div class="box">
               <div class="card wow fadeInLeft" data-wow-duration="1.8s">
                   <img src="nd_kimcom_partner/images/1.png" alt="">
               </div>
               <div class="card wow fadeInRight" data-wow-duration="1.8s">
                   <img src="nd_kimcom_partner/images/2.png" alt="">
               </div>
           </div>
       </div>
    </section>

<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_partner/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';