<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_partner')){
class TNCP_nd_kimcom_partner extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_partner">
        <div class="la">
            <img src="<?= $this->getPath() ?>images/la.png" alt="">
        </div>
        <div class="dia">
            <img src="<?= $this->getPath() ?>images/dia.png" alt="">
        </div>
       <div class="container">
            <div class="doitac wow fadeInUp" data-wow-duration="1s">
                <a href="#">Đối Tác</a>
            </div>
           <div class="box">
               <div class="card wow fadeInLeft" data-wow-duration="1.8s">
                   <img src="<?= $this->getPath() ?>images/1.png" alt="">
               </div>
               <div class="card wow fadeInRight" data-wow-duration="1.8s">
                   <img src="<?= $this->getPath() ?>images/2.png" alt="">
               </div>
           </div>
       </div>
    </section>
<?php }
}
}
