<?php

/**

 * Created by PhpStorm.

 * User: Administrator

 * Date: 7/8/2018

 * Time: 9:21 AM

 */



if (!class_exists('TNCP_hd_kimcom_header')){

class TNCP_hd_kimcom_header extends TNCP_ToanNang{



protected $options = [

    'categories' => array(),

    'hotline_ban_hang' => '',

    'menu' => '',

];

function __construct()

{

    parent::__construct(__FILE__);

    parent::setOptions($this->options);

}



/*Add html to Render*/

public function render(){ ?>
<script type="text/javascript">
		var deleteLog = false;

		$(document).ready(function() {
	    	$('#pagepiling').pagepiling({
	    		menu: '#menu',
	    		anchors: ['page1', 'page2', 'page3'],
	    		navigation: {
		            'textColor': '#f2f2f2',
		            'bulletsColor': '#ccc',
		            'position': 'right',
		            'tooltips': ['Page 1', 'Page 2', 'Page 3', 'Page 4']
		        }
			});
	    });
    </script>

    <style>
    /* Style for our header texts
	* --------------------------------------- */
	h1{
		font-size: 5em;
		font-family: arial,helvetica;
		color: #fff;
		margin:0;
		padding:0;
	}

	/* Centered texts in each section
	* --------------------------------------- */
	.section{
		text-align:center;
	}


	/* Backgrounds will cover all the section
	* --------------------------------------- */
	#section1,
	#section2,
	#section3{
		background-size: cover;
	}

	/* Defining each section background and styles
	* --------------------------------------- */
	#section1{
		background-image: url(imgs/bg1.jpg);
	}
	#section2{
		background-image: url(imgs/bg2.jpg);
		padding: 6% 0 0 0;
	}
	#section3{
		background-image: url(imgs/bg3.jpg);
		padding: 6% 0 0 0;
	}
	#section3 h1{
		color: #000;
	}



	#section1 h1{
		position: absolute;
		left: 0;
		right: 0;
		margin: 0 auto;
		top: 30px;
		color: #fff;
	}

	#section2 .intro{
		position: absolute;
		left: 0;
		right: 0;
		margin: 0 auto;
		top: 30px;
	}
	#section2 h1,
	#section2 p{
		text-shadow: 1px 5px 20px #000;
	}

	#section3 h1,
	#section3 p{
		text-shadow: 1px 5px 20px #000;
		color: #fff;
	}

	#infoMenu li a{
			color: #fff;
	}
    </style>

	

	

    <header class="hd_kimcom_header">

        <div class="bg-repeat-2">

            <div class="container">

            <div class="row">

                <div class="col-xs-12 col-md-3">

                    <div class="logo-container">

                        <?php  if(strlen(az_box_logo_primary())>0){



                            echo  az_box_logo_primary();



                        }else{  ?>



                            <a href="#">



                                <img src="<?php echo $this->getPath()?>images/logo.png">



                            </a>



                        <?php  } ?>

                    </div>

                </div>

                <!-- <div class="col-xs-12 col-md-7 col-sm-8">

                    <div class="search-container">

                        <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>

                        <a href="<?php echo wc_get_cart_url()?>" data-product_total="<?php echo  WC()->cart->get_cart_contents_count(); ?>" title="<?php _e('Giỏ hàng', 'tn_component')?>" class="btn-cart toannang-cart-number-btn">

                            <i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="toannang-cart-number"><?php echo  WC()->cart->get_cart_contents_count(); ?></span>

                        </a>



                    </div>

                </div> -->

                <div class="col-xs-12 col-sm-9">
                    <ul class="ifo-top">
                        <li>
                            <span>Hotline:</span> 0965.828.220
                        </li>
                        <li>
                            <span>Email:</span> kimcomhcm@gmail.com
                        </li>
                    </ul>
                </div>

            </div>

        </div>

        </div>

    </header>
    <div class="fix-height-top"></div>

    <!-- Không xoá div dưới -->
    <div id="pagepiling">
	
<?php }

}

}

