<?php

/**

 * Created by PhpStorm.

 * User: Administrator

 * Date: 7/8/2018

 * Time: 9:21 AM

 */



if (!class_exists('TNCP_hd_kimcom_menu')){

class TNCP_hd_kimcom_menu extends TNCP_ToanNang{



protected $options = [

    'categories' => array(),

    'hotline_ban_hang' => '',

    'menu' => '',

];

function __construct()

{

    parent::__construct(__FILE__);

    parent::setOptions($this->options);

}



/*Add html to Render*/

public function render(){ ?>



    <header class="hd_kimcom_menu">

        <div class="bg-repeat-2">

            <div class="container">

            <div class="row">
                <div class="menu-container hidden-xs">

                    <div class="col-xs-12 col-md-9 pull-right">

                        <nav id="nav-main">

                            <?php if(!empty($this->getOption('menu'))){

                                $menus = $this->getOption('menu');

                                hc_get_menu($menus);

                            } ?>

                        </nav>

                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>

        </div>

        </div>

    </header>

<?php }

}

}

