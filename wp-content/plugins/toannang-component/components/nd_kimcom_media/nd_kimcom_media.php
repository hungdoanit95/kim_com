<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_media')){
class TNCP_nd_kimcom_media extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_media" >
       <div class="container">
           <div class="row">
               <div class="col-xs-12 col-sm-6 wow fadeInUp" data-wow-duration="1s">
                   <div class="card">
                       <div class="card-img">
                           <img src="<?= $this->getPath() ?>images/1.png" alt="media" title="media">
                       </div>
                       <div class="card-content">
                           <h2 class="title">
                               dịch vụ truyền thông media
                           </h2>
                           <div class="link">
                               <a href="#" class="xt">Xem Thêm</a>
                               <a href="#" class="datlich">Đặt Lịch Tư Vấn</a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-xs-12 col-sm-6 wow fadeInUp" data-wow-duration="3s">
                   <div class="card">
                       <div class="card-img">
                           <img src="<?= $this->getPath() ?>images/2.png" alt="media" title="media">
                       </div>
                       <div class="card-content">
                           <h2 class="title">
                               dịch vụ truyền thông media
                           </h2>
                           <div class="link">
                               <a href="#" class="xt">Xem Thêm</a>
                               <a href="#" class="datlich">Đặt Lịch Tư Vấn</a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
        <div class="dv wow fadeInUp" data-wow-duration="2s">
            <a href="#">Xem tất cả dịch vụ</a>
        </div>
    </section>
<?php }
}
}
