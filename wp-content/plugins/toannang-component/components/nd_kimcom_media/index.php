<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_media-css' type="text/css" href="nd_kimcom_media/assets/css.css"/>

    <section id="nd_kimcom_media" >
       <div class="container">
           <div class="row">
               <div class="col-xs-12 col-sm-6 wow fadeInUp" data-wow-duration="1s">
                   <div class="card">
                       <div class="card-img">
                           <img src="nd_kimcom_media/images/1.png" alt="media" title="media">
                       </div>
                       <div class="card-content">
                           <h2 class="title">
                               dịch vụ truyền thông media
                           </h2>
                           <div class="link">
                               <a href="#" class="xt">Xem Thêm</a>
                               <a href="#" class="datlich">Đặt Lịch Tư Vấn</a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-xs-12 col-sm-6 wow fadeInUp" data-wow-duration="3s">
                   <div class="card">
                       <div class="card-img">
                           <img src="nd_kimcom_media/images/2.png" alt="media" title="media">
                       </div>
                       <div class="card-content">
                           <h2 class="title">
                               dịch vụ truyền thông media
                           </h2>
                           <div class="link">
                               <a href="#" class="xt">Xem Thêm</a>
                               <a href="#" class="datlich">Đặt Lịch Tư Vấn</a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
        <div class="dv wow fadeInUp" data-wow-duration="2s">
            <a href="#">Xem tất cả dịch vụ</a>
        </div>
    </section>

<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_media/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';