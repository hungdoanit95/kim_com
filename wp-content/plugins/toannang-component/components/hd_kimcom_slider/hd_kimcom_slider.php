<?php

/**

 * Created by PhpStorm.

 * User: Administrator

 * Date: 7/8/2018

 * Time: 9:21 AM

 */



if (!class_exists('TNCP_hd_kimcom_slider')){

class TNCP_hd_kimcom_slider extends TNCP_ToanNang{



protected $options = [

    'slider' => '',

];

function __construct()

{

    parent::__construct(__FILE__);

    parent::setOptions($this->options);

}



/*Add html to Render*/

public function render(){ ?>
<div class="section" id="section1">
   <div class="hd_kimcom_slider">
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
               <div class="bgSlider">
           <img class="bg_slider wow flipInY" data-wow-duration=".8s" src="<?php echo $this->getPath()?>images/polygon.png; ?>" alt="">
            <div class="intro-left">
                <ul>
                    <li>
                        <img class="wow slideInRight" data-wow-duration=".8s" data-wow-delay=".2s" src="<?php echo $this->getPath()?>images/car1.png" alt="">
                    </li>
                    <li>
                        <img class="wow slideInRight" data-wow-duration=".8s" data-wow-delay=".4s" src="<?php echo $this->getPath()?>images/car2.png" alt="">
                    </li>
                    <li>
                        <img class="wow slideInRight" data-wow-duration=".8s" data-wow-delay=".6s" src="<?php echo $this->getPath()?>images/car3.png" alt="">
                    </li>
                </ul>
            </div>
            <div class="intro-right">
                <p class="wow bounceInDown" data-wow-duration=".8s" data-wow-delay=".1s" >Chào mừng quý khách đến với</p>
                <h2 class="wow bounceInDown" data-wow-duration=".8s" data-wow-delay=".3s">CÔNG TY TƯ VẤN<br>
                    & THIẾT KẾ<br>
                    THƯƠNG HIỆU<br>
                    KIMCOM<br>
                </h2>
                <div class="btn btn--blue btn--border wow bounceInDown" data-wow-duration=".8s" data-wow-delay=".5s">
                     <a href="dat-lich">Đặt Lịch Tư Vấn</a>
                </div>
                <div class="btn btn--black wow bounceInDown" data-wow-duration=".8s" data-wow-delay=".7s">
                <a href="dich-vu">Xem Tất Cả Dịch Vụ</a>
                </div>
                
            </div>
       </div>
               </div>
           </div>
       </div>
       
   </div>
   </div>
<?php }

}

}

