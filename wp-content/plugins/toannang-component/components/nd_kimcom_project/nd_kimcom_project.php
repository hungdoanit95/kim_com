<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_project')){
class TNCP_nd_kimcom_project extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_project">
        <div class="container">
            <h2 class="title wow fadeInUp" data-wow-duration="1s">
                các dự án lớn của kimcom
            </h2>
            <div class="slider wow fadeInUp" data-wow-duration="3s">
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/1.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/2.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/3.png" alt="a" title="a">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="<?= $this->getPath() ?>images/1.png" alt="a" title="a">
                    </a>
                </div>
            </div>
            <div class="anmt wow fadeInUp" data-wow-duration="4s">
                <a href="#">An nam mỹ tửu</a>
            </div>
        </div>
    </section>
<?php }
}
}
