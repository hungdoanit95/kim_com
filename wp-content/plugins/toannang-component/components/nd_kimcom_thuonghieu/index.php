<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/12/2018
 * Time: 9:43 AM
 */
if (!isset($sum_code) || !$sum_code)
    require_once '../../header.php';
?>
    <!-- Chen them thu vien css day neu can -->

    <!-- css rieng cho component -->
    <link rel="stylesheet" id='nd_kimcom_thuonghieu-css' type="text/css" href="nd_kimcom_thuonghieu/assets/css.css"/>

    <section id="nd_kimcom_thuonghieu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s">
                    <div class="card">
                        <div class="card-img">
                            <img src="nd_kimcom_thuonghieu/images/1.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1.8s">
                    <div class="card">
                        <div class="card-img">
                            <img src="nd_kimcom_thuonghieu/images/2.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="2.8s">
                    <div class="card">
                        <div class="card-img">
                            <img src="nd_kimcom_thuonghieu/images/3.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="3.8s">
                    <div class="card">
                        <div class="card-img">
                            <img src="nd_kimcom_thuonghieu/images/4.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="dv wow fadeInLeft" data-wow-duration="6.8s">
            <a href="#">Xem tất cả dịch vụ</a>
        </div>
    </section>
    </div>
<?php
/* chen them thu vien js day neu can */
$custom_js .= '

';
/* js call function cho component */
$custom_js .= '
<script language="javascript" type="text/javascript" src="nd_kimcom_thuonghieu/assets/script.js"></script>
';
if (!isset($sum_code) || !$sum_code)
    require_once '../../footer.php';