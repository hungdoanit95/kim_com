<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_nd_kimcom_thuonghieu')){
class TNCP_nd_kimcom_thuonghieu extends TNCP_ToanNang{

protected $options = [
    'categories' => array(),
    'hotline_ban_hang' => '',
    'menu' => '',
];
function __construct()
{
    parent::__construct(__FILE__);
    parent::setOptions($this->options);
}

/*Add html to Render*/
public function render(){ ?>

<!--nd_huynhthanh_header-->
<section id="nd_kimcom_thuonghieu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s">
                    <div class="card">
                        <div class="card-img">
                            <img src="<?= $this->getPath() ?>images/1.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1.8s">
                    <div class="card">
                        <div class="card-img">
                            <img src="<?= $this->getPath() ?>images/2.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="2.8s">
                    <div class="card">
                        <div class="card-img">
                            <img src="<?= $this->getPath() ?>images/3.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="3.8s">
                    <div class="card">
                        <div class="card-img">
                            <img src="<?= $this->getPath() ?>images/4.png" alt="giải pháp" title="giải pháp">
                        </div>
                        <div class="card-content">
                            <h2 class="title">
                                giải pháp thiết kế thương hiệu
                            </h2>
                            </h2>
                            <div class="link">
                                <a href="#" class="xt">Xem Thêm</a>
                                <a href="#" class="tuvan">Đặc lịch tư vấn</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="dv wow fadeInLeft" data-wow-duration="6.8s">
            <a href="#">Xem tất cả dịch vụ</a>
        </div>
    </section>
<?php }
}
}
